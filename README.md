[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_week3/badges/master/pipeline.svg)](https://gitlab.com/jeremymtan/jeremytan_ids721_week3/-/commits/master)
# CDK TypeScript S3 Bucket

## Purpose of Project 
The purpose of the project is to create infrastructure as code for an S3 bucket. This is a starting point to help us deploy AWS services effectively without having to use the AWS console.

## S3 Bucket Properties 

![Screenshot_2024-02-08_at_4.14.07_PM](/uploads/72f35337c70dfe056885ddcd5f223966/Screenshot_2024-02-08_at_4.14.07_PM.png)

1. I add versioning by enabling it via `versioned: true`: 

![Screenshot_2024-02-08_at_4.11.12_PM](/uploads/385c78c56fcf10e5f6fbd19671be9647/Screenshot_2024-02-08_at_4.11.12_PM.png)

2. I add encryption by enabling it via `encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED`.

![Screenshot_2024-02-15_at_12.30.13_PM](/uploads/1a044139007b15328608a05fd2e0ef8d/Screenshot_2024-02-15_at_12.30.13_PM.png)

### Preparation
1. Have access to Codecatalyst (free is fine).
2. Connect a builder ID (needed for AWS Codewhisperer and Codecatalyst).
3. Create an empty dev environment.
4. Check if CDK is installed via `cdk --version`.
5. Now create an IAM user to use with Codecatalyst to deploy the S3 bucket.
6. Go to `IAM`, add a user, then add `IAMFullAccess` and `AmazonS3FullAccess` policies.
7. After finishing that, add an inline policy, then type `CloudFormation` and give access to all the commands.
8. After finishing that, add an inline policy, then type `Systems Manager` and give access to all the commands.
9. Create an access key and ID, save somewhere safe.
10. Type `aws configure` in the dev environment.
11. Fill out the AWS access key fields and leave the rest blank (note: you can fill the region but that may cause problems later on).
12. Create a project by first making a directory and then `cd` into it, then type `cdk init app --language=typescript`.
13. You now have a CDK template project, make your bucket or whatever you desire.
14. After you're done making your project, check it works properly with `npm run build`.
15. Create the CloudFormation template by typing `cdk synth`.
16. Deploy your CloudFormation template by typing `cdk deploy`.
17. If it fails to deploy, you need to attach a role to let it deploy.
18. First, create a JSON file, then copy `zombie.json`.
19. Find the error and identify the error role/`copy what comes after this`.
20. Type `export ZOMBIE_ROLE=the_role_name_in_the_error_message`.
21. Then create it `aws iam create-role --role-name=$ZOMBIE_ROLE --assume-role-policy-document file://zombie.json`.
22. Then attach it `aws iam attach-role-policy --role-name $ZOMBIE_ROLE --policy-arn arn:aws:iam::aws:policy/AdministratorAccess`.
23. Then try to deploy again `cdk deploy`.
24. Go to the AWS console and type in `S3` to see your bucket live.

## AWS Codewhisperer 
I use the following prompts to generate the bucket:
`// make an S3 bucket and add bucket properties like versioning and encryption` and `// add necessary vars to make my S3 bucket get created`.
Overall, it's slow since you have to wait for Codewhisperer to read your comment and make a change. The wait time to generate one line seems to be 5 seconds just to generate one line. I think of this tool as more of a way to start your code and figure out how to finish the rest, not necessarily waiting 1 minute to finish it producing what it's trying to output. 

## References
1. [AWS CloudFormation Role ARN aws iam xxx is invalid or cannot be assumed](https://thewerner.medium.com/aws-cloud-formation-role-arn-aws-iam-xxx-is-invalid-or-cannot-be-assumed-14c17e1098e2)
2. [AWS CDK S3 Bucket](https://towardsthecloud.com/aws-cdk-s3-bucket)
3. [AWS CDK Getting Started Guide](https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html)
4. https://aws.amazon.com/blogs/apn/building-cross-account-deployment-in-gitlab-pipelines-using-aws-cdk/

